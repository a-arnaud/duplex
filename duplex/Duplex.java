package duplex;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Collections;


class Rue {
    int A, B, D, C, L;
    int indice;
    boolean dejaVisite;
    public Rue(int A, int B, int D, int C, int L, int indice) {
        this.A = A;
        this.B = B;
        this.D = D;
        this.C = C;
        this.L = L;
        this.indice = indice;
        this.dejaVisite=false;
    }
    
    public double getRentabilite() {
        return ((double) L) / ((double) C);
    }
}

class Chemin {
    ArrayList<Rue> rues;
    int temps;
    int valeur;
    
    public Chemin() {
        rues = new ArrayList<Rue>();
        temps=0;
        valeur=0;
    }
    
    public void addStreet(Rue r) {
        rues.add(r);
        temps+=r.C;
        valeur+= r.dejaVisite? 0 : r.L;
    }
    
    public void removeStreet (Rue r) {
        rues.remove(r);
        temps-=r.C;
        valeur-= r.dejaVisite? 0 : r.L;
    }
    
    public double rentabilite() {
        return ((double) valeur) / ((double) temps);
    }
    
    public Chemin copy() {
        Chemin c=new Chemin();
        c.rues = (ArrayList<Rue>) this.rues.clone();
        c.temps=this.temps;
        c.valeur = this.valeur;
        return c;
    }
}


class Intersection {
    float latitude, longitude;
    int indice;
    ArrayList<Rue> ruesSortantes;
    
    // Pour dijkstra
    int distance = Integer.MAX_VALUE;
    Intersection intersection_precedente;
    Rue rue_precedente;
    
    public Intersection(float latitude, float longitude, int indice) {
        this.latitude = latitude;                    
        this.longitude = longitude;
        this.indice=indice;
        this.ruesSortantes= new ArrayList<Rue>();
    }
}

class Voiture {
    int indice;
    Intersection position;
    int instantArriveeProchaineIntersection;
    LinkedList<Intersection> visitees;
    
    public Voiture(int indice) {
        this.indice=indice;
        this.instantArriveeProchaineIntersection=0;
        this.position = null; //Le mettre à s au début! Fait dans initialiser
        this.visitees = new LinkedList<Intersection>();
    }
}

class VoitureComparateur implements Comparator<Voiture> {

    public VoitureComparateur() {
    
    }

    public int compare(Voiture v1, Voiture v2) {
        if(v1.instantArriveeProchaineIntersection < v2.instantArriveeProchaineIntersection) {
            return -1;
        }
        if(v1.instantArriveeProchaineIntersection > v2.instantArriveeProchaineIntersection) {
            return 1;
        }
        if(v1.indice < v2.indice) {
            return -1;
        }
        return 1;
    }
}

public class Duplex {

    static HashMap<Integer, Rue> rues = new HashMap<Integer, Rue>();
    static HashMap<Integer, Intersection> intersections = new HashMap<Integer, Intersection>();
    static ArrayList<LinkedList<Integer>> intersections_visitees = new ArrayList<LinkedList<Integer>>();
    static int N, M, T, C, S;
    static PriorityQueue<Voiture> voitures;
    static int score = 0;
    
    public static void initialiser() {
        intersections_visitees = new ArrayList<LinkedList<Integer>>();
        for (int i = 0; i < C; i++) {
            LinkedList<Integer> listWithStartingPoint = new LinkedList<Integer>();
            listWithStartingPoint.add(S);
            intersections_visitees.add(listWithStartingPoint);
        }
        score = 0;
        voitures = new PriorityQueue<Voiture>(C, new VoitureComparateur());
        for(Rue rue : rues.values()) {
            rue.dejaVisite = false;
        }
        
        //Initialisation des voitures:
        for (int c=0; c<C; c++) {
            Voiture v = new Voiture(c);
            v.position = intersections.get(S);
            voitures.add(v);
        }
        
        //Tri des rues sortantes par rentabilité décroissante
        for(Intersection intersection : intersections.values()) {
            Collections.sort(intersection.ruesSortantes, new Comparator<Rue>() {
                public int compare(Rue r1, Rue r2) {
                    return Integer.signum((int) (r2.getRentabilite() - r1.getRentabilite()));
                }
            });
        }
    }
    
    public static void algo1() {
        while (!voitures.isEmpty()) {
            Voiture v = voitures.poll();
            choisirDestination(v);
        }
    }
    
    public static void choisirDestination(Voiture v) {
        choisirDestination1(v);
        int avance = 3;
        //choisirDestinationEnAvance(v, avance);
    }
    
    public static boolean choisirDestination1(Voiture v) {
        //Choisir la rue non flaggee la plus rentable
        Rue bestRue = null;
        LinkedList<Rue> ruesEncorePossibles = new LinkedList<Rue>();
       
        //On suppose les rues sortantes triées par ordre décroissant de rentabilité
        for (Rue r: v.position.ruesSortantes) {
            if (v.instantArriveeProchaineIntersection + r.C < T) {
                ruesEncorePossibles.add(r);
                if (!r.dejaVisite) {
                bestRue = r;
                break;
                }
            }
        }
        if (bestRue==null && !ruesEncorePossibles.isEmpty()) bestRue = ruesEncorePossibles.get((int) Math.floor(Math.random()*Integer.MAX_VALUE)%ruesEncorePossibles.size());
        if (bestRue==null) return false;
        deplacerVoiture(v, bestRue);
        voitures.add(v);
        return true;
    }
    
       public static void choisirDestinationEnAvance(Voiture v, int avance) {
        Chemin bestPath= CheminRec(new Chemin(), avance, v.position);
        
        deplacerVoitureSurChemin(v, bestPath);
    }
    
    
    public static Chemin CheminRec (Chemin debutChemin, int avance, Intersection currentPos) {
        if (avance ==0) {
            return debutChemin;
        }
        else {
            Chemin bestPathStep = debutChemin;
            
            for (Rue r: currentPos.ruesSortantes) {
                debutChemin.addStreet(r);
                boolean oldBool=r.dejaVisite;
                r.dejaVisite=true;
                if (debutChemin.rentabilite()>bestPathStep.rentabilite()) {
                    bestPathStep = debutChemin.copy();
                }
                
                int indice_nouvelle_intersection = r.B;
                if(r.A != currentPos.indice) {
                    indice_nouvelle_intersection = r.A;
                }
                Intersection newPos = intersections.get(indice_nouvelle_intersection);
                Chemin bP = CheminRec(debutChemin, avance-1, newPos);
                if (bP.rentabilite() > bestPathStep.rentabilite()) {
                    bestPathStep = bP.copy();
                }
                
                r.dejaVisite=oldBool;
                debutChemin.removeStreet(r);  
            }
            return bestPathStep;
        }
    }
    
    public static void deplacerVoitureSurChemin(Voiture v, Chemin path) {
        boolean b= true;
        for (Rue r: path.rues) {
            if (v.instantArriveeProchaineIntersection + r.C < T) {
                deplacerVoiture(v, r);
                voitures.add(v);
            }
            else b = false;
            if (!b) {
                while (choisirDestination1(v)) {
                    voitures.remove(v);
                };
            }
        }
    }

    
    public static void algoAleatoire() {
        Voiture[] voitures = new Voiture[8];
        for (int i = 0; i < 8; i++) {
            voitures[i] = new Voiture(i);
            voitures[i].position = intersections.get(S);
        }

        for (Voiture voiture : voitures) {
            int t = 0;
            while (t < T) {
                ArrayList<Rue> ruesSortantes = voiture.position.ruesSortantes;
                Rue rue = ruesSortantes.get((int)(Math.random() * ruesSortantes.size()));
                t += rue.C;
                if (t < T) {
                    deplacerVoiture(voiture, rue);
                }
            }
        }
    }
    
    public static void algoAlexia(int[] temps, int[] indices_reperes) {
    int index_voiture = 0;
    for (Voiture voiture : voitures) {
    int score_max = 0;
    LinkedList<Rue> rues_choisies = null;
    for (int essai = 0; essai < 1000; essai++) {
    int score = 0;
        int t = temps[index_voiture];
        LinkedList<Rue> rues = new LinkedList<Rue>();
        LinkedList<Rue> rues_a_demarquer = new LinkedList<Rue>();
    while (t < T) {
                    ArrayList<Rue> ruesSortantes = voiture.position.ruesSortantes;
                    Rue rue = ruesSortantes.get((int)(Math.random() * ruesSortantes.size()));
                    t += rue.C;
                    if (t < T) {
                    rues.add(rue);
                   
                    int indice_nouvelle_intersection = rue.B;
                        if(rue.A != voiture.position.indice) {
                            indice_nouvelle_intersection = rue.A;
                        }
                        if (!rue.dejaVisite) {
                        rues_a_demarquer.add(rue);
                        score += rue.L;
                        }
                        rue.dejaVisite=true;
                        voiture.position = intersections.get(indice_nouvelle_intersection);
                    }
                }
    if (score > score_max) {
    score_max = score;
    rues_choisies = rues;
    }
    for (Rue rue : rues_a_demarquer) {
    rue.dejaVisite = false;
    }
    voiture.position = intersections.get(indices_reperes[index_voiture]);
    }
   
    for (Rue rue : rues_choisies) {
    deplacerVoiture(voiture, rue);
    }
   
    index_voiture++;
    }
    }
    
    // Renvoie les temps écoulés pour chaque voiture
    public static int[] dijkstra(int[] indices_reperes) {
    
        for(Intersection intersection : intersections.values()) {
            intersection.distance = Integer.MAX_VALUE;
            intersection.intersection_precedente = null;
            intersection.rue_precedente = null;
        }
        
        Intersection intersection_courante = intersections.get(4516);
        intersection_courante.distance = 0;
        PriorityQueue<Intersection> queue = new PriorityQueue<Intersection>(intersections.size(),
        new Comparator<Intersection>() {
                public int compare(Intersection i1, Intersection i2) {
                    return Integer.signum(i1.distance - i2.distance);
                }
            });
        
        for(Intersection intersection : intersections.values()) {
            queue.add(intersection);
        }
        while(!queue.isEmpty()) {
            intersection_courante = queue.poll();
            if(intersection_courante.distance == Integer.MAX_VALUE) {
                break;
            }
            for(Rue rue : intersection_courante.ruesSortantes) {
                Intersection intersection_voisine = intersections.get(rue.A);
                if(intersection_courante.indice == rue.A) {
                    intersection_voisine = intersections.get(rue.B);
                }
                int alt = intersection_courante.distance + rue.C;
                if(alt < intersection_voisine.distance) {
                    intersection_voisine.distance = alt;
                    intersection_voisine.intersection_precedente = intersection_courante;
                    intersection_voisine.rue_precedente = rue;
                    queue.remove(intersection_voisine);
                    queue.add(intersection_voisine);
                }
            }
        }
        
        int[] temps = new int[8];
        
        for(int indice_repere = 0; indice_repere < 8; indice_repere++) {
            LinkedList<Rue> rues = new LinkedList<Rue>();
            Intersection intersection_actuelle = intersections.get(indices_reperes[indice_repere]);
            while(intersection_actuelle.indice != 4516) {
                rues.addFirst(intersection_actuelle.rue_precedente);
                intersection_actuelle = intersection_actuelle.intersection_precedente;
            }
            
            Voiture[] voituresArray = voitures.toArray(new Voiture[2]);
            
            for (Rue rue : rues) {
                    temps[indice_repere] += rue.C;
                deplacerVoiture(voituresArray[indice_repere], rue);
            }
            
            voituresArray[indice_repere].instantArriveeProchaineIntersection = temps[indice_repere];
        }
        
        return temps;
    }
    
    public static void deplacerVoiture(Voiture v, Rue r) {
        int indice_nouvelle_intersection = r.B;
        if(r.A != v.position.indice) {
            indice_nouvelle_intersection = r.A;
        }
        if(!r.dejaVisite)
            score += r.L;
        r.dejaVisite=true;
        intersections_visitees.get(v.indice).addLast(indice_nouvelle_intersection);
        v.position = intersections.get(indice_nouvelle_intersection);
        v.instantArriveeProchaineIntersection += r.C;
    }
    
    public static void ecriture() {
        System.out.println(C);
        for(LinkedList<Integer> intersections : intersections_visitees) {
            System.out.println(intersections.size());
            for(Integer intersection : intersections) {
                System.out.println(intersection);
            }
        }
    }
    
    public static double deg2rad(double deg) {
      return (deg * Math.PI / 180.0);
    }
    
    public static double rad2deg(double rad) {
      return (rad * 180.0 / Math.PI);
    }
    
    public static double latLongDistance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        return dist;
    }
    
    public static int indiceZoneProche(Intersection intersection) {
        int result = -1;
        int[] indices_reperes = {2892, 5186, 4987, 662, 8062, 9183, 3076, 1489};
        double min = 9999999;
        int indice = 0;
        for (int indice_repere : indices_reperes) {
            Intersection repere = intersections.get(indice_repere);
            double dist = latLongDistance(intersection.latitude, intersection.longitude, repere.latitude, repere.longitude);
            if (dist < min) {
                min = dist;
                result = indice;
            }
            indice++;
        }
        return result;
    }
    
    public static void algoArnaudApresDijkstra() {
        int indice_voiture = 0;
        for (Voiture voiture : voitures) {
            indice_voiture++;
            int t = 0;
            while (t < T) {
                ArrayList<Rue> ruesSortantes = voiture.position.ruesSortantes;
                
                int indice_prochaine_zone;
                Rue rue;
                
                do {
                    rue = ruesSortantes.get((int)(Math.random() * ruesSortantes.size()));                   int indice_nouvelle_intersection = rue.B;
                    if(rue.A != voiture.position.indice) {
                        indice_nouvelle_intersection = rue.A;
                    }
                    Intersection nouvelle_intersection = intersections.get(indice_nouvelle_intersection);
                    indice_prochaine_zone = indiceZoneProche(nouvelle_intersection);
                } while (indice_prochaine_zone != indice_voiture);
                
                
                t += rue.C;
                if (t < T) {
                    deplacerVoiture(voiture, rue);
                }
            }
        }
    }
    
    public static void algoArnaudApresDijkstra(int[] temps) {
        int indice_voiture = 0;
        for (Voiture voiture : voitures) {
            System.out.println(voiture.position.indice);
            System.out.println(voiture.position.latitude);
            System.out.println(voiture.position.longitude);
            int t = temps[indice_voiture];
            while (t < T) {
                ArrayList<Rue> ruesSortantes = voiture.position.ruesSortantes;
                
                int indice_prochaine_zone;
                Rue rue;
                int iter = 0;
                
                do {
                    iter++;
                    rue = ruesSortantes.get((int)(Math.random() * ruesSortantes.size()));                   int indice_nouvelle_intersection = rue.B;
                    if(rue.A != voiture.position.indice) {
                        indice_nouvelle_intersection = rue.A;
                    }
                    Intersection nouvelle_intersection = intersections.get(indice_nouvelle_intersection);
                    indice_prochaine_zone = indiceZoneProche(nouvelle_intersection);
                    
                    if (indice_prochaine_zone != indice_voiture) {
                        System.out.println("échec "+iter+" "+indice_prochaine_zone+" "+indice_voiture);
                    }
                } while (indice_prochaine_zone != indice_voiture);
                
                System.out.println("succès");
                t += rue.C;
                if (t < T) {
                    deplacerVoiture(voiture, rue);
                }
            }
            indice_voiture++;
        }
    }
    
    public static void intersectionLaPlusProche(double latitude, double longitude) {
        Intersection best = null;
        double min = 9999999;
        for (int i = 0; i < N; i++) {
            Intersection intersection = intersections.get(i);
            double distance = latLongDistance(latitude, longitude, intersection.latitude, intersection.longitude);
            if (distance < min) {
                min = distance;
                best = intersection;
            }
        }
        System.out.println(best.indice);
    }
    
    public static int rand(int n) {
        return (int) Math.floor(Math.random() * n);
    }
    
    public static void main(String[] args) {
        
        //lecture du fichier texte  
        try{
            InputStream ips=new FileInputStream("paris_54000.txt"); 
            InputStreamReader ipsr=new InputStreamReader(ips);
            BufferedReader br=new BufferedReader(ipsr);
            
            String[] premiere_ligne = br.readLine().split(" ");
            N = Integer.parseInt(premiere_ligne[0]);
            M = Integer.parseInt(premiere_ligne[1]);
            T = Integer.parseInt(premiere_ligne[2]);
            C = Integer.parseInt(premiere_ligne[3]);
            S = Integer.parseInt(premiere_ligne[4]);
            
            String ligne;
            int index_ligne = 0;
            
            while ((ligne=br.readLine())!=null){
               if (index_ligne < N) {
                   String[] intersection_infos = ligne.split(" ");
                   Intersection intersection = new Intersection(Float.parseFloat(intersection_infos[0]), Float.parseFloat(intersection_infos[1]), index_ligne);
                   intersections.put(index_ligne, intersection);
               } else {
                   String[] rue_infos = ligne.split(" ");
                   Rue rue = new Rue(Integer.parseInt(rue_infos[0]), Integer.parseInt(rue_infos[1]), Integer.parseInt(rue_infos[2]), Integer.parseInt(rue_infos[3]), Integer.parseInt(rue_infos[4]), index_ligne - N);
                   Intersection intersection1 = intersections.get(Integer.parseInt(rue_infos[0]));
                   Intersection intersection2 = intersections.get(Integer.parseInt(rue_infos[1]));
                   intersection1.ruesSortantes.add(rue);
                   if (Integer.parseInt(rue_infos[2]) == 2)
                       intersection2.ruesSortantes.add(rue);
                   rues.put(index_ligne - N, rue);
                   
               }
               index_ligne++;
            }
            br.close();
            int score_max = 0;
            ArrayList<LinkedList<Integer>> intersections_visitees_max = new ArrayList<LinkedList<Integer>>();
            for(int i = 0; i < 10000; i++) {
                initialiser();
                //int[] indices_reperes = {2892, 5186, 4987, 662, 8062, 9183, 3076, 1489};
                int[] indices_reperes = {rand(intersections.size()), rand(intersections.size()), rand(intersections.size()), rand(intersections.size()), rand(intersections.size()), rand(intersections.size()), rand(intersections.size()), rand(intersections.size())};
                dijkstra(indices_reperes);
                algo1();
                //System.out.println(score);
                if(score > score_max) {
                    score_max = score;
                    intersections_visitees_max = (ArrayList<LinkedList<Integer>>) intersections_visitees.clone();
                 }
            }
            intersections_visitees = (ArrayList<LinkedList<Integer>>) intersections_visitees_max.clone();
            //System.out.println(score_max);
            ecriture();
            
        }       
        catch (Exception e){
            System.out.println(e.toString());
        }

    }

}
